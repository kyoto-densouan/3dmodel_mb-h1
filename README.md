# README #

1/3スケールの日立 MB-H1風小物のstlファイルです。
スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。
元ファイルはAUTODESK Fusion360です。


***

# 実機情報

## メーカ
- 日立

## 発売時期
- 1983年

## 参考資料

- [懐かしのホビーパソコン紹介](https://twitter.com/i/events/866964646787375105)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_mb-h1/raw/e7123b2cb3cb2cde538b047715c4c00f277470c1/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_mb-h1/raw/e7123b2cb3cb2cde538b047715c4c00f277470c1/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_mb-h1/raw/e7123b2cb3cb2cde538b047715c4c00f277470c1/ExampleImage.png)
